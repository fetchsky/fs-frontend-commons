import urlify from '../src/urlify';
import testCasesExecutor from './utils';

/**
 * urlify test
 */

// Test case for urlify
const urlifyCase = () => {
  const input = 'deals-and-discount';
  const output = 'Deals And Discount';
  it("Testing for input: '" + input + "'", () => {
    expect(urlify(input)).toStrictEqual(output);
  });
};

const cases = [urlifyCase];
testCasesExecutor({ title: 'GenerateDuxKey Test Cases', cases });
