import generateDuxKey from '../src/generateDuxKey';
import testCasesExecutor from './utils';

/**
 * generateDuxKey test
 */

const props = { id: 21, city: 'karachi', country: 'pakistan' };

// Test case for single key (key as string)
const singleKeyCase = () => {
  const keys = 'city';

  const output = 'karachi';
  it('Single key (key as string)', () => {
    expect(generateDuxKey(props, keys)).toStrictEqual(output);
  });
};

// Test case for Multiple keys (keys as array)
const multipleKeyCase = () => {
  const keys = ['id', 'city'];
  const output = '21|karachi';
  it('Multiple keys (keys as array)', () => {
    expect(generateDuxKey(props, keys)).toStrictEqual(output);
  });
};

// Test case for Include keys
const includeKeysCase = () => {
  const keys = ['id', 'city'];
  const output = 'id:21|city:karachi';
  it('With includeKeys', () => {
    expect(generateDuxKey(props, keys, true)).toStrictEqual(output);
  });
};

const cases = [singleKeyCase, multipleKeyCase, includeKeysCase];
testCasesExecutor({ title: 'GenerateDuxKey Test Cases', cases });
