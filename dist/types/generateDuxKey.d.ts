/**
 * Generates dux key from given object.
 *
 * @param props The source object.
 * @param keys Desired key/s need to be selected from source data.
 * @param includeKeys If true, then property name will also reflect in output string (See example# 3).
 * @returns Returns the dux key string.
 * @example
 *
 * // ** Example# 1 (Single key) **
 * const props = { keyA: "apple", keyB: "orange" };
 * const keys = "keyA";
 * generateDuxKey(props, keys);
 * // => "apple"
 *
 *
 * // ** Example# 2 (Multiple keys) **
 * const props = { keyA: "apple", keyB: "orange", keyC: "banana" };
 * const keys = ["keyA", "keyC"];
 * generateDuxKey(props, keys);
 * // => "apple|banana"
 *
 *
 * // ** Example# 3 (Include keys) **
 * const props = { keyA: "apple", keyB: "orange", keyC: "banana" };
 * const keys = ["keyA", "keyC"];
 * generateDuxKey(props, keys, true);
 * // => "keyA:apple|keyC:banana"
 *
 */
export default function generateDuxKey(props: object, keys: string | string[], includeKeys?: boolean): string;
