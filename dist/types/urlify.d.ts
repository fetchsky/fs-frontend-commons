/**
 * Generates urlified string by removing ("-") and converting the final string to start case
 *
 * @param url Input URL string.
 * @returns Returns the urlified string.
 * @example
 *
 *urlify("deals-and-discount");
 * // => "Deals And Discount"
 *
 */
export default function urlify(url: string): string;
