"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var startCase_1 = __importDefault(require("lodash/startCase"));
/**
 * Generates urlified string by removing ("-") and converting the final string to start case
 *
 * @param url Input URL string.
 * @returns Returns the urlified string.
 * @example
 *
 *urlify("deals-and-discount");
 * // => "Deals And Discount"
 *
 */
function urlify(url) {
    return startCase_1.default(url.replace(/-/g, ' '));
}
exports.default = urlify;
//# sourceMappingURL=urlify.js.map